# KKIMModule

[![CI Status](https://img.shields.io/travis/yangpeng/KKIMModule.svg?style=flat)](https://travis-ci.org/yangpeng/KKIMModule)
[![Version](https://img.shields.io/cocoapods/v/KKIMModule.svg?style=flat)](https://cocoapods.org/pods/KKIMModule)
[![License](https://img.shields.io/cocoapods/l/KKIMModule.svg?style=flat)](https://cocoapods.org/pods/KKIMModule)
[![Platform](https://img.shields.io/cocoapods/p/KKIMModule.svg?style=flat)](https://cocoapods.org/pods/KKIMModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KKIMModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KKIMModule'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

KKIMModule is available under the MIT license. See the LICENSE file for more info.
