//
//  main.m
//  KKIMModule
//
//  Created by yangpeng on 07/15/2022.
//  Copyright (c) 2022 yangpeng. All rights reserved.
//

@import UIKit;
#import "KKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KKAppDelegate class]));
    }
}
